# Back

Este proyecto es generado con ExpressJs version 4.17.1.

# Dependencias 

- nodemon (2.0.4): Paquete para lanzar el servidor y permitir cambios sin reiniciar
- morgan (1.10.0): Paquete para permitir visualizar el log de peticiones por consola
- jsonwebtoken (8.5.1): Paquete token de accesos
- helmet (4.1.1): Paquete que permite encriptar el trafico
- cors (2.8.5): Paquete para permitir cors del navegador
- express-validator (6.6.1): Paquete que valida el cuerpo de las peticiones
- typescript (4.0.2): Paquete para usar el lenguaje typescript

# Resumen
Este proyecto trabaja bajo una arquitectura tradicional de modelos, rutas y servicios. Ademas de las herramientas como lo son los middlewares, helpers entre otros.

### Explicación de la arquitectura
- config: Contiene información de configuracion de tokens
- data: Simulacion de la base de datos
- helpers: Los uso para simulacion de gestor de base de datos y generación de token
- middlewares: Seccion de codigo que permite validaciones antes de entrar a la rutas
- models: Son el modelado de los datos
- routes: Son los archivos del enrutamiento del aplicativo solo disponible para rutas hijas las rutas padres se almacenan en en el archivo app
- services: Es las que hacen las que contienen el negocio de la app
- app: Aqui se configura toda la app 
- index: Lanza el servidor

## Development server

Para lanzar el servidor se usa el comando `npm run dev` para el servidor de desarrollo con typescript gracias a ts-nod.

