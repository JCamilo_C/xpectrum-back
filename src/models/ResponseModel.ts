export class ResponseModel<T> {
    Response: boolean;
    Message: string;
    Result: T;

    constructor (Response: boolean = false, Message: string = "", Result: T) {
        this.Response = Response;
        this.Message = Message;
        this.Result = Result;
    }

    setResponse(response: boolean, message: string, result: T) {
        this.Response = response;
        this.Message = message;
        this.Result = result;
    }

    getResponse() {
        return { Response: this.Response, Message: this.Message, Result: this.Result }
    }
};


export interface Paginate<T> {
    pages: number;
    items: T;
    total: number;
}
