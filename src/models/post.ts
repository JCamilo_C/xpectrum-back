export interface PostDb {
    id: number;
    title: string;
    created_at: string;
    image: string;
    short_description: string;
    description: string;
    autor: string;
}

export interface Post {
    id: number;
    title: string;
    image: string;
    short_description: string;
    description: string;
    autor: string;
    created_at: Date;
}

export interface PostList {
    id: number;
    title: string;
    image: string;
    short_description: string;
    autor: string;
    created_at: Date;
}