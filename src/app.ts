import express, {Application} from 'express';
import morgan from 'morgan';
import cors from 'cors';
import helmet from "helmet";
import AuthRoutes from './routes/auth.routes';
import PostRoutes from './routes/post.routes';

export class App {

    private app: Application;
    private debug: boolean = false;

    constructor(private port?: number | string){
        this.app = express();
        this.settings();
        this.middlewares();
        this.routes();
        
    }

    // CONFIGURACION DE PUERTOS
    settings(){
        this.app.set(
            'port', this.port || process.env.PORT || 3000
        )
    }

    // CONFIGURACION THIRDS PARTY
    middlewares(){
        this.app.use(helmet());
        this.app.use(cors());
        this.app.use(express.json());
        this.app.use(express.urlencoded({extended: true}));

        // encriptacion trafico
        if(this.debug) {
            this.app.use(morgan('dev'));
        } else {
            this.app.use(morgan('short'));
        }
    }

    // RUTAS PADRES
    routes(){
        this.app.use('/api/auth', AuthRoutes);
        this.app.use('/api/post', PostRoutes);
    }

    async listen(){
        await this.app.listen(this.app.get('port'));
        console.log(`Server on url http://localhost:${this.app.get('port')}` );
    }

}