import { NextFunction, Request, Response } from "express";
import { ResponseModel } from '../models/ResponseModel';
import * as jwt from 'jsonwebtoken';
import * as config from '../config/config'

export interface IGetUserAuthInfoRequest extends Request {
    user?: string; // or any other type
    idUser?: number // or any other type
  }

export const validatejwt = (req: IGetUserAuthInfoRequest, res: Response, next: NextFunction) => {
    const token = req.header('x-token');
    const rm = new ResponseModel<null>(false, "Se necesita un token valido", null);
    if (!token) {
        return res.status(401).json(rm.getResponse());
    }

    try {
        const payload = jwt.verify(token, config.secretKey) as { userId: number, username: string};
        req.user = payload.username;
        req.idUser = payload.userId;
        next();   
    } catch (error) {
        rm.Message = "Token no valido"
        return res.status(401).json(rm.getResponse());   
    }
}