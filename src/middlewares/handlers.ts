import { NextFunction, Request, Response } from 'express';
import { validationResult } from 'express-validator';
import { ResponseModel } from '../models/ResponseModel';

export const validateResquest = (req: Request, res: Response, next: NextFunction) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }

    next();
}


export const handleError = (res: Response, error: any) => {
    console.error(error)
    let rm = new ResponseModel<null>(
        false,
        "A ocurrido un error inesperado con el servidor",
        null
    );

    return res.status(500).json(rm.getResponse());
}