/**
 * path: /api/auth
 */
import { Router } from 'express';
import authController from '../services/auth.service';
import { body } from 'express-validator'
import { validateResquest } from '../middlewares/handlers';


const router: Router = Router();

// Creacion de rutas
router.get('/:token', authController.RefreshToken)
router.post('/login',[
    body('email', 'Debe ingresar un correo valido').isEmail(),
    body('password', 'Debe ingresar una contraseña valida').notEmpty(),
    validateResquest
], authController.Login)


export default router;