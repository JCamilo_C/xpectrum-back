import { Router } from 'express';
import postService from '../services/post.service';
import { body } from 'express-validator'
import { validateResquest } from '../middlewares/handlers';
import { validatejwt } from '../middlewares/validate-jwt';


const router: Router = Router();

// Creacion de rutas
router.get('/last/:size', [validatejwt], postService.GetLast)
router.get('/all', [validatejwt], postService.GetAll)
router.get('/detail/:id', [validatejwt], postService.GetDetail)
router.post('/create', [
    body('title', 'El titulo del post es requerido').notEmpty(),
    body('short_description', 'La descripción corta del post es requerida').notEmpty(),
    body('description', 'La descripción del post es requerida').notEmpty(),
    body('image', 'La imagen del post es requerido').notEmpty(),
    body('autor', 'El nombre del autor del post es requerido').notEmpty(),
    validateResquest,
    validatejwt
], postService.CreatePost)
router.put('/update', [
    body('id', 'El id del post es requerido').notEmpty(),
    body('title', 'El titulo del post es requerido').notEmpty(),
    body('short_description', 'La descripción corta del post es requerida').notEmpty(),
    body('description', 'La descripción del post es requerida').notEmpty(),
    body('created_at', 'La fecha de creación del post es requerido').notEmpty(),
    body('image', 'La imagen del post es requerido').notEmpty(),
    body('autor', 'El nombre del autor del post es requerido').notEmpty(),
    validateResquest,
    validatejwt
], postService.UpdatePost)

export default router;