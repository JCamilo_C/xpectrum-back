import { JsonWebTokenError } from 'jsonwebtoken';
import * as config from '../config/config'
import * as jwt from "jsonwebtoken";
import { User } from '../models/User';

export const GenerateToken = (email: string, id: number) => {
    //Sing JWT, valid for 1 hour
    const token = jwt.sign(
        { userId: id, username: email },
        config.secretKey,
        { expiresIn: "2h" }
    );
    
    return token;
}