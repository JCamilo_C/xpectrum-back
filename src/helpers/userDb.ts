import * as path from 'path';
import { readFileSync } from 'fs';
import { User } from '../models/User';

const ReadUsers = (): User[] => {
    const path_data = path.join(__dirname, '../data/user.json');
    let rawdata = readFileSync(path_data);
    return JSON.parse(rawdata.toString());
}

export const FindUser = (email: string, password: string) => {
    const users = ReadUsers();
    const user = users.find(u => u.email === email)   
    if (user?.password == password) return user;
    else return null
}