import PostJson from '../data/post.json';
import { PostList, PostDb, Post } from '../models/post';
import { writeFileSync, readFileSync } from 'fs';
import * as path from 'path'
import { Paginate } from '../models';

class Mappers {

    mapPostToPostList(p: Post): PostList {
        return {
            id: p.id,
            autor: p.autor,
            image: p.image,
            created_at: new Date(p.created_at),
            short_description: p.short_description,
            title: p.title
        }
    }

    mapJsonToPostList(p: PostDb): PostList {
        return {
            id: p.id,
            autor: p.autor,
            image: p.image,
            created_at: new Date(p.created_at),
            short_description: p.short_description,
            title: p.title
        }
    }

    mapJsonToPost(p: PostDb): Post {
        return {
            id: p.id,
            autor: p.autor,
            image: p.image,
            created_at: new Date(p.created_at),
            short_description: p.short_description,
            title: p.title,
            description: p.description
        }
    }

    mapPostToPostDb(p: Post): PostDb  {
        return {
            id: p.id,
            autor: p.autor,
            image: p.image,
            created_at: p.created_at.toISOString(),
            short_description: p.short_description,
            title: p.title,
            description: p.description
        }
    }
}

const ReadPost = (): PostDb[] => {
    const path_data = path.join(__dirname, '../data/post.json');
    let rawdata = readFileSync(path_data);
    return JSON.parse(rawdata.toString());
}

export const GenerateId = () => {
    return PostJson.length + 1;
}

export const GetLastPosts = (size: number) => {
    const m = new Mappers();
    const posts = ReadPost();
    return posts.map(m.mapJsonToPostList).sort((a, b) => b.created_at.getTime() > a.created_at.getTime() ? 1 : -1).slice(0,size);
}

export const GetAllPost = (page: number, size: number): Paginate<PostList[]> => {
    const m = new Mappers();
    const posts = ReadPost();
    let total = posts.length;
    let pages = Math.ceil(total / size);
    let start = (page - 1) * size;
    let end = page * size;
    const query = posts.map(m.mapJsonToPost).sort((a, b) => b.created_at.getTime() > a.created_at.getTime() ? 1 : -1).slice(start,end)
    const response: Paginate<PostList[]> = {
        pages,
        total,
        items: query
    }

    return response
}

export const GetPost = (id: number) => {
    const m = new Mappers();
    const posts = ReadPost();
    const p = posts.find(p => p.id === id);
    if (p) return m.mapJsonToPost(p)    
    else return;
}

export const CreatePost = (payload: Post) => {
    const m = new Mappers();
    payload.id = GenerateId();
    payload.created_at = new Date();
    try {
        const db = ReadPost();
        db.push(m.mapPostToPostDb(payload));
        const path_data = path.join(__dirname, '../data/post.json');
        writeFileSync(path_data, JSON.stringify(db))
        return m.mapPostToPostList(payload);
    } catch (error) {
        console.error(error);
        return null;
    }
}

export const UpdatePost = (payload: Post) => {
    const m = new Mappers();
    payload.created_at = typeof(payload.created_at) === 'string' ? new Date(payload.created_at) : payload.created_at;
    const updated_post = m.mapPostToPostDb(payload);
    try {
        const posts = ReadPost();
        const db = posts.map(p => {
            if (p.id === updated_post.id) {
                p = {...updated_post};
            }
            return p;
        });
        const path_data = path.join(__dirname, '../data/post.json');
        writeFileSync(path_data, JSON.stringify(db));
        return payload;
    } catch (error) {
        console.error(error);
        return null;
    }
}