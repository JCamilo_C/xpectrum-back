/**
 * path: /api/post
 */
import { Response} from 'express';
import { Paginate, ResponseModel } from '../models/ResponseModel';
import { GetAllPost, GetLastPosts, GetPost, CreatePost, UpdatePost } from '../helpers/postDb';
import { handleError } from '../middlewares/handlers';
import { IGetUserAuthInfoRequest } from '../middlewares/validate-jwt';
import { Post, PostList } from '../models/post';


class PostService {
    public async GetAll(req: IGetUserAuthInfoRequest, res: Response) : Promise<Response>  {
        const rm = new ResponseModel<Paginate<PostList[]> | null>(false, "", null);
        const page = Number(req.query.page);
        try {
            const post = GetAllPost(page, 10);
            if (post) {
                rm.setResponse(true, "", post);
            } else {
                rm.setResponse(true, "No se encontro ningún post", null);
            }

            return res.status(200).json(rm.getResponse());
        } catch (error) { return handleError(res, error) }
    }

    public async GetLast(req: IGetUserAuthInfoRequest, res: Response) : Promise<Response>  {
        const rm = new ResponseModel<PostList[] | null>(false, "", null);
        const size: number = Number(req.params.size);
        try {
            const post = GetLastPosts(size);
            if (post) {
                rm.setResponse(true, "", post);
            } else {
                rm.setResponse(true, "No se encontro ningún post", null);
            }

            return res.status(200).json(rm.getResponse());
        } catch (error) { return handleError(res, error) }
    }

    public async GetDetail(req: IGetUserAuthInfoRequest, res: Response) : Promise<Response>  {
        const rm = new ResponseModel<Post | null>(false, "", null);
        const id_post: number = Number(req.params.id);
        try {
            const post = GetPost(id_post);
            if (post) {
                rm.setResponse(true, "", post);
            } else {
                rm.setResponse(true, "No se encontro ningún post", null);
            }

            return res.status(200).json(rm.getResponse());
        } catch (error) { return handleError(res, error) }
    }

    public async CreatePost(req: IGetUserAuthInfoRequest, res: Response) : Promise<Response>  {
        const rm = new ResponseModel<PostList |  null>(false, "", null);
        const payload: Post = req.body;
        try {
            const post = CreatePost(payload);
            if (post) {
                rm.setResponse(true, `Se guardo exitosamente el post ${post.title}`, post);
            } else {
                rm.setResponse(true, "A ocurrido un error al momento de guardar", null);
            }

            return res.status(200).json(rm.getResponse());
        } catch (error) { return handleError(res, error) }
    }

    public async UpdatePost(req: IGetUserAuthInfoRequest, res: Response) : Promise<Response>  {
        const rm = new ResponseModel<null>(false, "", null);
        const payload: Post = req.body;
        try {
            const post = UpdatePost(payload);
            if (post) {
                rm.setResponse(true, `Se edito exitosamente el post ${post.title}`, null);
            } else {
                rm.setResponse(false, "A ocurrido un error al momento de guardar", null);
            }

            return res.status(200).json(rm.getResponse());
        } catch (error) { return handleError(res, error) }
    }

}

const Post = new PostService();
export default Post;


