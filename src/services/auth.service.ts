import { Request,  Response} from 'express';
import { ResponseModel } from '../models/ResponseModel';
import { Authentication } from '../models/User';
import { FindUser } from '../helpers/userDb';
import { GenerateToken } from '../helpers/jwt';
import { handleError } from '../middlewares/handlers';
import { IGetUserAuthInfoRequest } from '../middlewares/validate-jwt';
import * as jwt from 'jsonwebtoken';

class AuthService {

    public async Login(req: Request, res: Response) : Promise<Response>  {
        const rm = new ResponseModel<Authentication | null>(false, "", null);
        const {email, password} = req.body;
        try {
            const user = FindUser(email, password);
            if (user) {
                rm.setResponse(true, "", { token: GenerateToken(user.email, user.id), user: user });
            } else {
                rm.setResponse(true, "Las credenciales ingresadas son erroneas", null);
            }

            return res.status(200).json(rm.getResponse());
        } catch (error) { return handleError(res, error) }
    }

    public async RefreshToken(req: IGetUserAuthInfoRequest, res: Response) : Promise<Response>  {
        const token = req.params.token;
        const rm = new ResponseModel<string | null>(false, "", null);
        try {
            const payload = jwt.decode(token) as { userId: number, username: string, exp: any};
            if (payload) {
                rm.setResponse(true, "", GenerateToken(payload.username, payload.userId));
                return res.status(200).json(rm.getResponse());
            } else {
                rm.Message = "El token no es valido";
                return res.status(400).json(rm.getResponse());
            }
        } catch (error) {
            console.log('error', error)
            return res.status(400).json(rm.getResponse());
        }
    }


}

const Auth = new AuthService();
export default Auth;


